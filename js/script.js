
$(document).ready( function() {
    
    var modify = null;
    //array dels tags trobats al elastisearch al buscador
    var availableTags = [];   
   
    //creacio de preguntas
    $("#sendPlainJSon").click(function(e) {
        var formEmpty = isEmpty()
       if(formEmpty){
        alert("formulari correcte");
        doPut();
        location.reload();
       } else {
        alert("formulari incorrecte");
       }
    }); 
    
    //omple el formulari despres de buscar amb el autocomplete y guarda la id i el nom del event en una variable per al update/delete del formulari
    
    $("#search").click(function(e) {
       modify = fillForm($(this).attr('name'),$('#tags').val());
       alert(modify)
    });

    //si la variable id (encara no s'ha buscat cap pregunta), no fara ningun post i sortira un alert amb l'error
    $("#update").click(function(e) {
        
        if(modify == null) {
            alert("error 404: _id not found");
        } else {
            doPost(modify);
            modify = null;
            location.reload();
        }
    });

     $("#delete").click(function(e) {
        if(modify == null) {
            alert("error 404: _id not found");
        } else {
            doDelete(modify);
            modify = null;
            location.reload();
        }
    });

     //utilització del plugin css per el sidebar
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });    

    //utilització del plugin de autocomplete per al buscador de preguntas
    $('#tags').keyup(function(e){
       availableTags = doGet();
       $( "#tags" ).autocomplete({
      source: availableTags     
    });
    });   

 });


//metode per crear preguntes a partir d'un mapping predeterminat en la funcio createJSON
function doPut() {

    $.ajax({

        type: "POST",
        async: false,
        url: 'http://localhost:9200/trivial/questions/',
        data: JSON.stringify(createJSON()),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (msg) 
                { 
                    alert("the json created is :"+ "\n"+
                        "index : " + msg._index+"\n"+
                        "type : " + msg._type+"\n"+
                        "id : " + msg._id);                    
                },
        error: function (err)
        { alert(err.responseText)}
    });
};

function doDelete(){
    alert('working')
};

//metode per fer update
function doPost(id){
     $.ajax({

        type: "POST",
        async: false,
        url: 'http://localhost:9200/trivial/questions/'+id,
        data: JSON.stringify(createJSON()),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (msg) 
                { 
                    alert("the json updated is :"+ "\n"+
                        "index : " + msg._index+"\n"+
                        "type : " + msg._type+"\n"+
                        "id : " + msg._id);                    
                },
        error: function (err)
        { alert(err.responseText)}
    });
};

//metode per fer delete
function doDelete(id){
    $.ajax({

        type: "DELETE",
        async: false,
        url: 'http://localhost:9200/trivial/questions/'+id,
        data: JSON.stringify(createJSON()),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        success: function (msg) 
                { 
                    alert("the json deleted is :"+ "\n"+
                        "index : " + msg._index+"\n"+
                        "type : " + msg._type+"\n"+
                        "id : " + msg._id);                    
                },
        error: function (err)
        { alert(err.responseText)}
    });
};

//metode per fer bsuquedas a partir del metode createQueryAsJSON
function doGet(){   

    var result = [];   

    $.ajax({
      method: "POST",
      url: "http://localhost:9200/trivial/questions/_search",
      crossDomain: true,  
      async: false,
      data: JSON.stringify(createQueryAsJSON()),
      dataType : 'json',
      contentType: 'application/json',
      success: function(data) {
                $.each(data.hits.hits, function(i,item){
                    //guarda la id i el text de la pregunta per utilitzarles al plugin de autocomplete i fer update/delete per id al formulari                  
                    var id = item._id;
                    var text = item._source.question_text;
                    var tmp = {
                        value: id,
                        label: text};

                    result.push(tmp);                    
                }) 

        },
    error: function (err)
        { alert(err.responseText)}
    });   

    return result;
}

//metode per fer bsuquedas a partir del metode createQueryAsJSON
function getList(){    
    
    $('#tableBody').empty();
    
    $.ajax({
      method: "POST",
      url: "http://localhost:9200/trivial/questions/_search",
      crossDomain: true,  
      async: false,
      data: JSON.stringify(all()),
      dataType : 'json',
      contentType: 'application/json',
      success: function(data) {
                $.each(data.hits.hits, function(i,item){                                   
                    
                    var text = item._source.question_text;
                    var a = item._source.options[0].A;
                    var b = item._source.options[1].B;
                    var c = item._source.options[2].C;
                    var d = item._source.options[3].D;
                    var response = item._source.correct;
                    var category = item._source.category;
                    var level = item._source.level;
                    
                    $('#tableBody').append("<tr>"+
                                                "<td>"+i+"</td>"+
                                                "<td>"+text+"</td>"+
                                                "<td>"+a+"</td>"+
                                                "<td>"+b+"</td>"+
                                                "<td>"+c+"</td>"+
                                                "<td>"+d+"</td>"+
                                                "<td>"+response+"</td>"+
                                                "<td>"+category+"</td>"+
                                                "<td>"+level+"</td>"+
                                            "</tr>");                    
                }) 

        },
    error: function (err)
        { alert(err.responseText)}
    }); 
}

function getListFiltered(filter){
    
    $('#tableBody').empty();
    
    $.ajax({
      method: "POST",
      url: "http://localhost:9200/trivial/questions/_search",
      crossDomain: true,  
      async: false,
      data: JSON.stringify(filteredQuery(filter)),
      dataType : 'json',
      contentType: 'application/json',
      success: function(data) {
                $.each(data.hits.hits, function(i,item){                                   
                    
                    var text = item._source.question_text;
                    var a = item._source.options[0].A;
                    var b = item._source.options[1].B;
                    var c = item._source.options[2].C;
                    var d = item._source.options[3].D;
                    var response = item._source.correct;
                    var category = item._source.category;
                    var level = item._source.level;
                    
                    $('#tableBody').append("<tr>"+
                                                "<td>"+i+"</td>"+
                                                "<td>"+text+"</td>"+
                                                "<td>"+a+"</td>"+
                                                "<td>"+b+"</td>"+
                                                "<td>"+c+"</td>"+
                                                "<td>"+d+"</td>"+
                                                "<td>"+response+"</td>"+
                                                "<td>"+category+"</td>"+
                                                "<td>"+level+"</td>"+
                                            "</tr>");                    
                }) 

        },
    error: function (err)
        { alert(err.responseText)}
    }); 
}

//omple el formulari mitjançant una busqueda directa al elastic per id i retorna la id per a fer el update/delete al elastic
function fillForm(method, id){
    
    var id;

    $.ajax({
      method: "GET",
      url: "http://localhost:9200/trivial/questions/"+id,
      crossDomain: true,  
      async: false,      
      dataType : 'json',
      contentType: 'application/json',
      success: function(data) {
                               
                        //setters de los campos del objeto encontrado que queremos segun si es delete/update
                if(method == "update"){                                                       
                    $('#question').val(data._source.question_text).prop("disabled", false);
                    $('#A').val(data._source.options[0].A).prop("disabled", false);
                    $('#B').val(data._source.options[1].B).prop("disabled", false);
                    $('#C').val(data._source.options[2].C).prop("disabled", false);
                    $('#D').val(data._source.options[3].D).prop("disabled", false);
                    $('#correct').val(data._source.correct).prop("disabled", false);
                    $('#category').val(data._source.category).prop("disabled", false);
                    $('#level').val(data._source.level).prop("disabled", false);

                    id = data._id;
                }
                if(method == "delete"){
                    $('#question').val(data._source.question_text);
                    $('#A').val(data._source.options[0].A);
                    $('#B').val(data._source.options[1].B);
                    $('#C').val(data._source.options[2].C);
                    $('#D').val(data._source.options[3].D);
                    $('#correct').val(data._source.correct);
                    $('#category').val(data._source.category);
                    $('#level').val(data._source.level);

                    id = data._id;
                }         
    },
    error: function (err)
        { alert(err.responseText)}
    });

    return id;      
} 

//fa les consultes al elastic a partir de les paraules filtradas conicidents
function createQueryAsJSON() {

    var filter = $('#tags').val();

    var query = {
            "query": {
                "query_string": {
                    "query": filter
                }
            }
        };       

    return query;
};

function all(){  

    var query = {
            "query": {
                "match_all": {}
            }
        };       

    return query;

}

function filteredQuery(filter){

    var query = {
           "query": {
        "query_string" : {
            "default_field" : "category",
            "query" : filter
            }
        }
    };
        
    return query;
}

function isEmpty(){

    var isFormValid = true;    

    if($.trim($(".well textarea").val()).length == 0){
        $(".well textarea").parent().addClass("has-error");
        isFormValid = false;
        $(".well textarea").focus();
        return isFormValid;
    } else {
         $(".well textarea").parent().removeClass("has-error");
    }

    $(".well input").each(function(){        
        if ($.trim($(this).val()).length == 0){
            $(this).parent().addClass("has-error");
            isFormValid = false;
            $(this).focus();
            return isFormValid;
        }
        else{
            $(this).parent().removeClass("has-error");
        }
    });

    return isFormValid;
}

function createJSON() {    
    
    var jsonObj = {};
    var options = [];

    //get de los input del html
        var question = $('#question').val(); 
        var A = $('#A').val();
        var B = $('#B').val();
        var C = $('#C').val();
        var D = $('#D').val();
        var correct = $('#correct').val();
        var category = $('#category').val();
        var level = $('#level').val();
        var url = "null";
    
    //Set del json en el formato que queremos
        jsonObj.question_text = question;
        options.push({"A" : A});
        options.push({"B" : B});
        options.push({"C" : C});
        options.push({"D" : D});
        jsonObj.options = options;
        jsonObj.correct = correct;
        jsonObj.category = category;
        jsonObj.level = level;
        jsonObj.imageURL = url;        
        
    return jsonObj; 
};






